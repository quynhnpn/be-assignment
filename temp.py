from jinja2 import Template

fields = []
data = {'id':1, "name": "Hung", "team": None, "age": None}
for key, value in data.items():
    if value != None:
        fields.append(key)
template = Template(open('templates.txt').read()) 
print(template.render(fields = fields, data = data, temp = None))