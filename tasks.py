from celery import Celery
import sys, os
from workers.add_user import receive_query

#app = Celery('tasks', backend='redis://localhost:6379', broker='amqp://localhost:5672')
app = Celery('tasks', backend='redis://localhost:6379', broker='pyamqp://guest@localhost//')


@app.task
def add_user():
    try:
        receive_query()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

