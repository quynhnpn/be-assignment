from typing import Tuple
from xmlrpc.client import boolean
import psycopg2
import redis
import json
import uuid
import time
from jinja2 import Template
import connexion
from flask import jsonify, request, Response

from workers.send import send_msg_to_rabbitmq


# app = Flask(__name__)


def create_response(
        data: dict = None, status: int = 200, message: str = "", using_cache: boolean = False, tracking_id: str = "",
) -> Tuple[Response, int]:
    if type(data) is not dict and data is not None:
        raise TypeError("Data should be a dictionary 😞")

    response = {
        "code": status,
        "success": 200 <= status < 300,
        "message": message,
        "result": data,
        "using_cache": using_cache,
        "tracking_id": tracking_id
    }
    return jsonify(response)


def get_db_connection():
    conn = psycopg2.connect(host='localhost',
                            database='postgres',
                            user='postgres',
                            password='postgres')
    return conn


def get_users():
    conn = get_db_connection()
    cur = conn.cursor()

    r_key = request.url
    r_value = r.get(r_key)

    # If cached
    if r_value is not None:
        data = json.loads(r_value)
        return create_response({"user": data}, using_cache=True)
    else:

        args = request.args.to_dict()
        fields = []
        for key, value in args.items():
            if value is not None:
                fields.append(key)

        # request users with query
        if fields:

            template = Template(open('template/get_user_template.txt').read())
            cur.execute(template.render(fields=fields, data=args))
        # Dont have query == request all users
        else:
            cur.execute('SELECT * FROM users;')

        users = cur.fetchall()

        keys = ["id", "age", "name", "team"]
        data = []

        for e in users:
            user = dict(zip(keys, e))
            data.append(user)
        if not data:
            return create_response({'error': "404: User not found"}, status=404)

        r_key = request.url
        r_value = json.dumps(data)
        r.set(r_key, r_value, ex=30)

        cur.close()
        conn.close()
        return create_response({"user": data})


def post_user():
    tracking_id = str(uuid.uuid1())
    value = "In-progress"
    r.set(str(tracking_id), value, ex=1000)

    status = r.get(tracking_id)
    data = request.get_json(force=True)

    query = {"method": "POST", "data": data, "tracking_id": tracking_id}

    body = json.dumps(query)
    send_msg_to_rabbitmq(key=tracking_id, body=body)

    return create_response({"data": status.decode()}, tracking_id=tracking_id, using_cache=True)


def check_if_user_exist(id):
    conn = get_db_connection()
    cur = conn.cursor()

    cur.execute('SELECT COUNT(*) FROM users u WHERE u.id = %s;', (id,))
    id = cur.fetchone()[0]

    if id != 0:
        return True
    return False


def get_user_by_id(id):
    conn = get_db_connection()
    cur = conn.cursor()

    r_key = request.url
    r_value = r.get(r_key)

    # value is in cache
    if r_value is not None:
        data = json.loads(r_value)
        return create_response({"user": data}, using_cache=True)
    # value is not in cache
    else:
        user_exist = check_if_user_exist(id)
        if user_exist:
            cur.execute('SELECT * FROM users u WHERE u.id = %s;', (id,))
            resp = cur.fetchone()

            keys = ["id", "age", "name", "team"]
            data = dict(zip(keys, resp))

            r_key = request.url
            r_value = json.dumps(data)
            r.set(r_key, r_value, ex=30)

            cur.close()
            conn.close()
            time.sleep(10)
            return create_response({"user": data})
        else:
            return create_response({'error': "404: User not found"}, status=404)


def put_user_by_id(id):
    conn = get_db_connection()
    cur = conn.cursor()

    user_exist = check_if_user_exist(id)
    if user_exist:

        # args = request.args.to_dict()
        data = request.get_json(force=True)
        print(data)

        fields = []
        for key, value in data.items():
            if value is not None:
                fields.append(key)

        if fields:
            template = Template(open('template/mod_user_by_ID_template.txt').read())
            print(template.render(fields=fields, data=data, id=id))
            cur.execute(template.render(fields=fields, data=data, id=id))
            conn.commit()

            cur.close()
            conn.close()

        return create_response({'msg': "User is modified successfully."}, status=201)
    else:
        return create_response({'error': "404: User not found"}, status=404)


def del_user_by_id(id):
    conn = get_db_connection()
    cur = conn.cursor()

    user_exist = check_if_user_exist(id)
    if user_exist:
        cur.execute('DELETE FROM users WHERE id = %s;' % id)
        conn.commit()
        cur.close()
        conn.close()
        return create_response({'msg': "User is deleted successfully."})
    else:
        return create_response({'error': "404: User not found"}, status=404)


def tracking_by_id(id):
    status = r.get(id)
    print(status)
    if status is not None:
        return create_response({"status": status.decode()}, tracking_id=id, using_cache=True)
    else:
        return create_response({"error": "dont have this tracking id in cache"}, tracking_id=id, using_cache=True)


r = redis.Redis(
    host='localhost',
    port=6379)
app = connexion.App(__name__)
app.add_api('swagger.yaml')
# set the WSGI application callable to allow using uWSGI:
# uwsgi --http :8080 -w app
application = app.app

if __name__ == '__main__':
    # run our standalone gevent server
    app.run(port=8080, server='gevent')
