import os
import psycopg2

conn = psycopg2.connect(
    host="localhost",
    database="postgres",
    user='postgres',
    password='postgres')

# Open a cursor to perform database operations
cur = conn.cursor()

# Execute a command: this creates a new table
cur.execute('DROP TABLE IF EXISTS users;')
cur.execute("CREATE TABLE users (id serial NOT NULL PRIMARY KEY,"
            "age integer NOT NULL,"
            "name varchar NOT NULL,"
            "team varchar NOT NULL);"
            )

# Insert data into the table

cur.execute('INSERT INTO users (age, name, team)'
            'VALUES (%s, %s, %s)',
            (19,
             "Aria",
             "LWB")
            )

cur.execute('INSERT INTO users (age, name, team)'
            'VALUES (%s, %s, %s)',
            (20,
             "Tim",
             "LWB")
            )

cur.execute('INSERT INTO users (age, name, team)'
            'VALUES (%s, %s, %s)',
            (23,
             "Varun",
             "NNB")
            )

cur.execute('INSERT INTO users (age, name, team)'
            'VALUES (%s, %s, %s)',
            (24,
             "Alex",
             "C2TC")
            )


conn.commit()

cur.close()
conn.close()
